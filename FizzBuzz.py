class Solution(object):
    def fizzBuzz(self, n):   
        arr = [ ]
        for i in range(1 , n + 1 , 1):
            a = i % 3
            b = i % 5
            if a == 0 and b == 0 : arr.append("FizzBuzz")
            elif a == 0: arr.append("Fizz")
            elif b == 0: arr.append("Buzz")
            else : arr.append(str(i))
        return arr
        